import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import Channel from "./components/Channel";
import DetailedVideo from "./components/DetailedVideo";
import DetailedChannel from "./components/DetailedChannel";
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
    const [videos, setVideos] = useState([]);
    const [selectedVideo, selectVideo] = useState({});
    const [channels, setChannels] = useState([]);
    const [selectedChannel, selectChannel] = useState({});
    const [selectedType, selectType] = useState("videos");

    return (
        <Container className="p-3">
            <Router>
            <MyNav onResults={setVideos} selectedType={selectedType} selectType={selectType}/>
                <Switch>
                    <Route path="/videos/:videoId">
                        <DetailedVideo id={selectedVideo.id}
                                       snippet={selectedVideo.snippet} player={selectedVideo.player}
                                       statistics={selectedVideo.statistics} />
                    </Route>
                    <Route path="/channels/:channelId">
                        <DetailedChannel id={selectedChannel.id}
                                       snippet={selectedChannel.snippet} player={selectedChannel.player}
                                       statistics={selectedChannel.statistics} />
                    </Route>
                    <Route path="/search/videos/:search">
                        <>
                            {videos.map(v => {
                                console.log(v);
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                if (selectedType === "videos")  {
                                    return (<Video
                                        videoId={v.id.videoId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishTime={publishTime}
                                        title={title}
                                        selectVideo={selectVideo}/>);
                                } else{
                                    return (<Channel
                                        channelId={v.id.videoId}
                                        thumbnail={thumbnails.high}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishTime={publishTime}
                                        title={title}
                                        selectChannel={selectChannel}/>);
                                }
                                    
                                    
                                    
                                    
                            })}
                        </>
                    </Route>
                    <Route path="/">
                        Merci d'effectuer une recherche...
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
